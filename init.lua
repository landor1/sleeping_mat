-- Sleeping Mat
-- by David G (kestral246@gmail.com)
-- 2020-01-29
-- Nordal 10.5.2020 changed craft recipes

-- junglegrass
minetest.override_item("default:junglegrass", {
	groups = {snappy = 3, flora = 1, attached_node = 1, flammable = 1, grass_craft = 1},
})

-- grass
minetest.override_item("default:grass_1", {
    groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1,not_in_creative_inventory=1, grass = 1, grass_craft = 1},
})

for i = 2, 5 do
	minetest.override_item("default:grass_" .. i, {
groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1,not_in_creative_inventory=1, grass = 1, grass_craft = 1},
	})
end

--dry grass
minetest.override_item("default:dry_grass_1", {
    groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1,not_in_creative_inventory=1, dry_grass = 1, grass_craft = 1},
})

for i = 2, 5 do
	minetest.override_item("default:dry_grass_" .. i, {
groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1,not_in_creative_inventory=1, dry_grass = 1, grass_craft = 1},
	})
end

-- marram grass
minetest.override_item("default:marram_grass_1", {
    groups = {snappy = 3, flammable = 3, attached_node = 1, grass_craft = 1},
})

for i = 2, 3 do
    minetest.override_item("default:marram_grass_" .. i, {
        groups = {snappy = 3, flammable = 3, attached_node = 1, not_in_creative_inventory=1, grass_craft = 1},
	})
end

beds.register_bed("sleeping_mat:mat", {
	description = ("Grass Sleeping Mat"),
	inventory_image = "sleeping_mat_roll.png",
	wield_image = "sleeping_mat_roll.png",
	tiles = {
		bottom = {
			"sleeping_mat_top1.png",
			"sleeping_mat_top1_side.png",
		},
		top = {
			"sleeping_mat_top2.png",
			"sleeping_mat_top2_side.png",
		},
	},
	nodebox = {
		bottom = {
			{-1/2, -1/2, -1/2, 1/2, -7/16, 1/2}
		},
		top = {
			{-1/2, -1/2, -1/2, 1/2, -7/16, 1/2}
		},
	},
	selectionbox = {-1/2, -1/2, -1/2, 1/2, -7/16, 3/2},

	recipe = {
		{"group:grass_craft", "group:grass_craft", "group:grass_craft"},
		{"group:grass_craft", "group:grass_craft", "group:grass_craft"},
		{"group:grass_craft", "group:grass_craft", ""},
	},
})

